# API-Exercise

I'm using python3 in the flask framework to host the API and I am using a Mysql db as the backend.
The Mysql is configured as a StatefulSet and is set up for replication and failover.
I tested this in a GKE cluster on version 1.12.7, though they should run without issue on any version later than 1.9 (when StatefulSets were fully introduced)
The StatefulSets use PVC templates and thus require a Storage Class that supports Dynamic Provisionning.


### 1. Create images

3 images needs to be created; the first image (db-loader.py) is used to run a job which will populate our database using the provided .csv file.
The actual API is broken into 2 images so that we can deploy 2 different deployements, this allows us to make changes to parts of the API without taking the entire thing down.
We will be using the publicly available image for the Mysql StatefulSet, so there is no need to prepare an image for it.

### 2. Prepare the Cluster environment

Before deploying the Mysql or API pods, we need to create the namespace (for easy clean up), some configMaps and a secret. 
Make sure to modify the [username] and [set_password] fields. These will be used to create a db user which the APIs will consume to make queries.
  * kubectl apply -f env.yaml
  * kubectl create secret db-secrets --from-literal=user=[username] --from-literal=password=[set_password] -n titanic
  
### 3. Deploy and populate the Database

Before deploying, make sure to update the pop-db.yaml spec.template.spec.containers[0].image field with your repo/image name

  * kubectl apply -f mysql.yaml
  * kubectl apply -f pop-db.yaml
  
The Mysql pods will take time to initialise and spin up since the statefulset will create each pod in turn. 
The pop-db.yaml job has a readiness check to ensure the master node is running before populating the db and is configured to automatically delete 5 minutes after completion.

### 4. Deploy the API pods

Before deploying, make sure to update the both of the spec.template.spec.containers[0].image field with your repo/image name in the YAML.
Note that the first Deployment is for the "people-api" and the second is for the "person-api", inverting these two fields will cause problems with the ingress and traffic routing.

  * kubectl apply -f api.yaml
  
The Ingress resource will be provisioned using your default Ingress Controller. 
Once the Ingress obtains an IP address, the API is ready to start serving requests.